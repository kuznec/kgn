<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller_Common
{
    public $_page = 1;              //Текущая страница
    public $_n = 20;                //Количество новостей на странице
    
    public function action_index()
    {
        if($this->request->param('page'))
            $this->_page = $this->request->param('page');
        $start = $this->_n*$this->_page-$this->_n;
        $limit = $this->_n;
        
        $all_news = Model::factory('News')->get_all();
        $news = Model::factory('News')->get_all_limit($start, $limit);
        
        $paginator = Pagination::factory(array('total_items' => $all_news->count(), 'items_per_page' => $this->_n));
        $this->template->paginator = $paginator;
        
        $name = 'Новости Красноярска';
        $this->template->title = "Новости Красноярска - 24kgn.ru";
        $this->template->keywords = "";
        $this->template->description = "";
        $this->template->content = View::factory("news/all")
                ->set('news', $news)
                ->set('name', $name);
    }
    
    public function action_item()
    {
        $url = Request::current()->param('page');
        $news = Model::factory('News')->get_item($url);
        if($news->count() == 0)
        {
            throw new HTTP_Exception_404('Страница не найдена'); // Тададам!
            exit();
        }
        $this->template->name = $news[0]->name;
        $this->template->title = $news[0]->name." | Новости - 24kgn.ru";
        $this->template->keywords = "";
        $this->template->description = "";
        $this->template->content = View::factory("news/item")
                ->set('id', $news[0]->id)
                ->set('url', $news[0]->url)
                ->set('year', $news[0]->year)
                ->set('month', $news[0]->month)
                ->set('tag', $news[0]->tag)
                ->set('rubs_name', $news[0]->rubs_name)
                ->set('link', $news[0]->link)
                ->set('img_name', $news[0]->img_name)
                ->set('name', $news[0]->name)
                ->set('text', $news[0]->text)
                ->set('date', $news[0]->date);
    }
    
    public function action_rubric()
    {
        $rubric = $this->request->param('page');
        $start = $this->_n*$this->_page-$this->_n;
        $limit = $this->_n;
        $meta = Model::factory('News')->get_meta($rubric);
        $rubs = Model::factory('News')->info_group($rubric);
        $all_news = Model::factory('News')->get_by_rubs_all($rubric);
        $news = Model::factory('News')->get_by_rubs($rubric, $start, $limit);
        $paginator = Pagination::factory(array('total_items' => $all_news->count(), 'items_per_page' => $this->_n));
        $this->template->paginator = $paginator;
        $name = "Новости Красноярска в рубрике &laquo;".$rubs[0]->name."&raquo;";
        $this->template->title = "Новости Красноярска в рубрике &laquo;".$rubs[0]->name."&raquo; - 24kgn.ru";
        $this->template->keywords = $meta[0]->keywords;
        $this->template->description = $meta[0]->description;
        $this->template->content = View::factory("news/all")
                ->set('news', $news)
                ->set('name', $name);
    }
}