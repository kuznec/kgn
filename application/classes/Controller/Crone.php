<?php

defined('SYSPATH') or die('No direct script access.');

require APPPATH . 'classes/Include/Xml' . EXT;

class Controller_Crone extends Controller
{

    public function action_index()
    {
        $xml = file_get_contents('http://krsk.sibnovosti.ru/news/rss_mail.xml');
        $rss = new XML($xml);
        foreach($rss->rss->channel->item as $it)
        {
            echo $it->link."<br>";
            $list = Model::factory('Crone')->get_item($it->link);
            echo $list->count()."<br>";
            if($list->count() > 0)
                continue;
            else
            {
                Model::factory('Crone')->insert_item($it->link);
                $cat = 0;
                if(isset($it->category[0]))
                {
                    if($it->category[0] == "Экономика")
                        $cat = 2;
                    if($it->category[0] == "Происшествия")
                        $cat = 1;
                    if($it->category[0] == "Политика")
                        $cat = 3;
                    if($it->category[0] == "Общество")
                        $cat = 4;
                    if($it->category[0] == "Культура")
                        $cat = 5;
                    if($it->category[0] == "Экология")
                        $cat = 6;
                    if($it->category[0] == "Автоновости")
                        $cat = 7;
                    if($it->category[0] == "Путешествия")
                        $cat = 8;
                }
                $conre = $this->parse($it->link);
                $conds = explode("<div class='content'>", $conre);
                $conds2 = explode("</div>", $conds[1]);
                $con = $conds2[0];

                preg_match_all("#([0-9]{2}):([0-9]{2}) ([0-9]{2}).([0-9]{2}).([0-9]{4}).*#i", $conre, $date);
                $data = array('d' => date('d'), 'm' => date('n'), 'y' => date('Y'));
                $titles = $it->title;
                $img = @$it->enclosure['url'];
                $data_create = $data['d'] . "-" . $data['m'] . "-" . $data['y'];

                $cont = strip_tags($con, "<p>");
                $file_arr = array();

                $flag_img = 0;
                if($img != "")
                {
                    $wget_url = $img;
                    $tupes = explode(".", $img);
                    @mkdir(PUBPATH . "/images/news/" . $data['y'] . "/" . $data['m'] . "/", 0777, true);
                    $file_arr = array(
                        'wget_url' => $wget_url,
                        'dir' => PUBPATH . "/images/news/" . $data['y'] . "/" . $data['m'] . "/",
                        'name' => rand(11111111, 99999999) . "." . $tupes[(count($tupes) - 1)],
                        'month' => $data['m'],
                        'year' => $data['y'],
                    );
                    $flag_img = 1;
                }

                $name = $this->btw2($titles);
                $url = Controller_Static_Translit::set_translit($name);
                $text = $cont;
                $time = date("H:i:s");

                $link = "http://krsk.sibnovosti.ru";
                $fields = array('name', 'url', 'text', 'link', 'rubs', 'date', 'img');
                $values = array($name, $url, $text, $link, $cat, date('Y-m-d H:i:s', strtotime($data_create . " " . $time)), $flag_img);
                $last = Model::factory('Crone')->insert_news($fields, $values);
                $this->insert_photo($last[0], $file_arr);
            }
        }
    }

    private function insert_photo($id, $array)
    {
        if(!empty($array))
        {
            if(is_dir($array['dir']) && isset($array['name']) && $array['name'] != "")
            {
                $date = file_get_contents($array['wget_url']);
                $dir = $array['dir'] . $id . "_" . $array['name'];
                file_put_contents($dir, $date);
                Model::factory('Crone')->insert_img($id, $array['year'], $array['month'], $id . "_" . $array['name']);
            }
        }
    }

    private function parse($url)
    {
        return file_get_contents($url);
    }

    private function btw2($b1)
    {
        $b1 = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $b1);
        $b1 = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $b1);
        return $b1;
    }

}
