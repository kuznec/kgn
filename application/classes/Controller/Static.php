<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Static extends Controller_Common
{
    public $template = 'main';

    public function action_index()
    {
        $this->template->content = "";
        if($this->request->param('page') == "")
        {
            $news_last = Model::factory('News')->get_last(5);
            $popular = Model::factory('News')->popular(10);
            $slider = View::factory('slider')->set('slider', $news_last);
            
            $html_main = View::factory('pages/main')
                    ->set('slider', $slider)
                    ->set('popular', $popular);
            
            $this->template->content = $html_main;
        }
    }

} // End Static