<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cronecourses extends Controller
{

    public function action_index()
    {
        $this->action_courses();
        Controller_Static_Sitemap::index();
    }

    public function action_courses()
    {
        $currency = array('dollar' => 'R01235','euro' => 'R01239',);
        $courses = $this->get_rate($currency);
        $usd = number_format($courses['dollar']['today'], 2, ',', ' ');
        $eur = number_format($courses['euro']['today'], 2, ',', ' ');
        Model::factory('Courses')->set_courses($usd, $eur);
    }

    private function get_rate($currency)
    {
        $date = getdate();  // получаем ассоциативный массив с данными по дате
        $day_of_the_week = $date['wday']; // день недели
        //расчет коэффициента смещения
        switch($day_of_the_week)
        {
            case 0: $k1 = 2;
                $k2 = 1;
                break;   // воскресенье
            case 1: $k1 = 3;
                $k2 = 2;
                break;   // понедельник
            case 2: $k1 = 3;
                $k2 = 0;
                break;  // вторник
            default: $k1 = 1;
                $k2 = 0;
                break;   // среда, четверг, пятница, суббота
        }

        $month = $date['mon'];    // месяц
        $day = $date['mday'];     // число сегодня
        $yesterday = $day - $k1;  // число для получения курса на предыдущий день
        $today = $day - $k2;      // число для получения курса на сегодня
        $year = $date['year'];    // год
        $date_yesterday = date("d/m/Y", mktime(0, 0, 0, $month, $yesterday, $year)); // Генерация даты для курса предыдущего дня
        $date_today = date("d/m/Y", mktime(0, 0, 0, $month, $today, $year)); // Генерация даты для курса на сегодня
        $rate = array();

        foreach($currency as $key => $value)
        {
            $urls = 'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=' . $date_yesterday . '&date_req2=' . $date_today . '&VAL_NM_RQ=' . $value;
            $xml = simplexml_load_file($urls);
            $rate_today = round(str_replace(',', '.', $xml->Record->Value), 4);
            $rate_yesterday = round(str_replace(',', '.', $xml->Record[0]->Value), 4);
            $range = round($rate_today - $rate_yesterday, 4);
            if($range > 0)
            {
                $range = '+ ' . $range;
                $img = 'up';
            }
            elseif($range == 0)
            {
                $range = 0;
                $img = 0;
            }
            else
            {
                $range = '- ' . ($range * -1);
                $img = 'down';
            }
            $rate[$key] = array(
                'today' => $rate_today,
                'change' => $range,
                'img' => $img
            );
        }

        return $rate;
    }
}