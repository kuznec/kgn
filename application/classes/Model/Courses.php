<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Courses extends Model
{
    protected $_table = 'courses';
    
    public function set_courses($usd,$eur)
    {
        return DB::insert($this->_table, array('usd','eur'))
                ->values(array($usd,$eur))
                ->execute();
    }
    
    public function last()
    {
        return DB::select()
                ->from($this->_table)
                ->order_by('date','DESC')
                ->limit(1)
                ->as_object()
                ->execute();
    }
}