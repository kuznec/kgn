<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Crone extends Model
{
    protected $_table = 'news_cron';
    
    public function get_item($url)
    {
        return DB::select()
                ->from($this->_table)
                ->where('url','=',$url)
                ->as_object()
                ->execute();
    }
    
    public function insert_item($url)
    {
        DB::insert($this->_table, array('url'))
                ->values(array($url))
                ->execute();
    }
    
    public function insert_news($f, $d)
    {
        return DB::insert('news', $f)
                ->values($d)
                ->execute();
    }
    
    public function insert_img($id, $year, $month, $name)
    {
        DB::insert('news_img', array('news_id', 'year', 'month', 'name'))
                ->values(array($id, $year, $month, $name))
                ->execute();
    }
}