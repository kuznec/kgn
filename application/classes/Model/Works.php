<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Works extends Model
{
    public $_table = "works";
    
    public function get_all()
    {
        return DB::select()
                    ->from($this->_table)
                    ->where('status', '=', 1)
                    ->order_by('sort')
                    ->order_by('id', 'DESC')
                    ->as_object()
                    ->execute();
    }
    
    public function get_last()
    {
        return DB::select()
                    ->from($this->_table)
                    ->where('status', '=', 1)
                    ->order_by('id', 'DESC')
                    ->limit(1)
                    ->as_object()
                    ->execute();
    }
    
    public function get_work($name)
    {
        return DB::select()
                    ->from($this->_table)
                    ->where('translit', '=', $name)
                    ->as_object()
                    ->execute();
    }
}