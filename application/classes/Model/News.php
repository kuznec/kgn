<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_News extends Model
{
    protected $_table = 'news';
    
    public function get_all()
    {
        return DB::select()
                ->from($this->_table)
                ->join('news_img', 'left')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->join('news_rubs', 'left')
                ->on($this->_table.'.rubs','=','news_rubs.id')
                ->where($this->_table.'.status','=',1)
                ->order_by($this->_table.'.id', 'DESC')
                ->as_object()
                ->execute();
    }
    
    public function get_all_limit($start, $limit)
    {
        return DB::select($this->_table.".*", 'news_rubs.tag', DB::expr('news_rubs.name AS rubs_name'), 'news_img.year', 'news_img.month', DB::expr('news_img.name AS img_name'))
                ->from($this->_table)
                ->join('news_img', 'left')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->join('news_rubs', 'left')
                ->on($this->_table.'.rubs','=','news_rubs.id')
                ->where($this->_table.'.status','=',1)
                ->order_by('id', 'DESC')
                ->limit($limit)
                ->offset($start)
                ->as_object()
                ->execute();
    }
    
    public function get_last($limit)
    {
        return DB::select($this->_table.".*", 'news_img.year', 'news_img.month', DB::expr('news_img.name AS img_name'))
                ->from($this->_table)
                ->join('news_img')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->where($this->_table.'.status','=',1)
                ->order_by('id', 'DESC')
                ->limit($limit)
                ->as_object()
                ->execute();
    }
    
    public function popular($limit)
    {
        return DB::select($this->_table.".*", 'news_img.year', 'news_img.month', DB::expr('news_img.name AS img_name'))
                ->from($this->_table)
                ->join('news_img')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->where($this->_table.'.status','=',1)
                ->order_by('RAND()')
                ->limit($limit)
                ->as_object()
                ->execute();
    }
    
    public function get_item($url)
    {
        return DB::select($this->_table.".*", 'news_rubs.tag', DB::expr('news_rubs.name AS rubs_name'), 'news_img.year', 'news_img.month', DB::expr('news_img.name AS img_name'))
                ->from($this->_table)
                ->join('news_img', 'left')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->join('news_rubs', 'left')
                ->on($this->_table.'.rubs','=','news_rubs.id')
                ->where($this->_table.'.status','=',1)
                ->where($this->_table.'.url','=',$url)
                ->as_object()
                ->execute();
    }
    
    public function get_by_rubs_all($rubric)
    {
        return DB::select()
                ->from($this->_table)
                ->join('news_img', 'left')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->join('news_rubs', 'left')
                ->on($this->_table.'.rubs','=','news_rubs.id')
                ->where($this->_table.'.status','=',1)
                ->where('news_rubs.tag','=',$rubric)
                ->as_object()
                ->execute();
    }
    
    public function get_by_rubs($rubric, $start, $limit)
    {
        return DB::select($this->_table.".*", 'news_rubs.tag', DB::expr('news_rubs.name AS rubs_name'), 'news_img.year', 'news_img.month', DB::expr('news_img.name AS img_name'))
                ->from($this->_table)
                ->join('news_img', 'left')
                ->on($this->_table.'.id','=','news_img.news_id')
                ->join('news_rubs', 'left')
                ->on($this->_table.'.rubs','=','news_rubs.id')
                ->where($this->_table.'.status','=',1)
                ->where('news_rubs.tag','=',$rubric)
                ->order_by('date', 'DESC')
                ->limit($limit)
                ->offset($start)
                ->as_object()
                ->execute();
    }
    
    public function info_group($group)
    {
        return DB::select()
                ->from('news_rubs')
                ->where('tag', '=', $group)
                ->as_object()
                ->execute();
    }
    
    public function get_meta($rubs)
    {
        return DB::select()
                ->from('pages')
                ->where('translit', '=', $rubs)
                ->where('status', '=', 1)
                ->as_object()
                ->execute();
    }
}