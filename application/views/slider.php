<div class="flexslider">
    <ul class="slides"><?php
        foreach($slider as $k => $v) { ?>
            <li itemscope itemtype="http://schema.org/ImageObject">
                <a title="<?= $v->name; ?>" href="<?= URL::base(); ?>news/item/<?= $v->url; ?>.html">
                    <img itemprop="contentUrl" src="<?= URL::base(); ?>public/images/news/<?= $v->year; ?>/<?= $v->month; ?>/<?= $v->img_name; ?>" alt="<?= $v->name; ?>" title="<?= $v->name; ?>"></a>
                <div class="flex-caption">
                    <div class="caption_title_line">
                        <p itemprop="name"><?= $v->name; ?></p>
                    </div>
                </div>
            </li><?php
        } ?>
    </ul>
</div>