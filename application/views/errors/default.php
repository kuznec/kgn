<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>404 - Страница не найдена</title>
        <link rel="stylesheet" href="<?= URL::base(); ?>public/css/normalize.css">
        <link rel="stylesheet" href="<?= URL::base(); ?>public/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= URL::base(); ?>public/css/main.css?v=2">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?= URL::base(); ?>public/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <div class="container">
                <div class="row top">
                    <div class="col-md-7 col-sm-12 col-xs-12 logo"><label><a title="Красноярские городские новости" href="<?= URL::base(); ?>">Красноярские городские новости</a></label></div>
                    <div class="col-md-2 col-sm-4 col-xs-4"><p>Курсы валют:</p><p>$ <?= $usd; ?></p><p>€ <?= $eur; ?></p></div>
                    <div class="col-md-3 col-sm-8 col-xs-8">
                        <noindex><a href="https://clck.yandex.ru/redir/dtype=stred/pid=7/cid=1228/*https://yandex.ru/pogoda/62" target="_blank"><img src="https://info.weather.yandex.net/62/2_white.ru.png?domain=ru" border="0" alt="Яндекс.Погода"/><img width="1" height="1" src="https://clck.yandex.ru/click/dtype=stred/pid=7/cid=1227/*https://img.yandex.ru/i/pix.gif" alt="" border="0"/></a></noindex>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default top-navbar" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Меню</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav main-nav  clear navbar-right ">
                                <li><a title="Главная" href="/">Главная</a></li>
                                <li><a title="Новости" href="/news">Новости</a></li>
                                <li><a title="Происшествия" href="/news/rubric/incidents">Происшествия</a></li>
                                <li><a title="Экономика" href="/news/rubric/business">Экономика</a></li>
                                <li><a title="Политика" href="/news/rubric/politics">Политика</a></li>
                                <li><a title="Общество" href="/news/rubric/society">Общество</a></li>
                                <li><a title="Культура" href="/news/rubric/culture">Культура</a></li>
                                <li><a title="Экология" href="/news/rubric/ekologiya">Экология</a></li>
                                <li><a title="Автоновости" href="/news/rubric/auto">Автоновости</a></li>
                                <li><a title="Путешествия" href="/news/rubric/travels">Путешествия</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <section class="static">
                <div class="container">
                    <h1>404 - Страница не найдена</h1>
                    <p>Такая страница не найдена.</p>
                    <p>Попробуйте воспользоваться навигацией сверху, чтобы найти нужную для Вас информацию.</p>
                </div>
            </section>
        </div>
        <?= $footer; ?>
        <script type="text/javascript" src="<?= URL::base(); ?>public/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="<?= URL::base(); ?>public/js/bootstrap.min.js" ></script>
    </body>
</html>