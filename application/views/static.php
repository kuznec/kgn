<section class="static">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= $name; ?></h1>
                <?= $text; ?>
            </div>
        </div>
    </div>
</section>