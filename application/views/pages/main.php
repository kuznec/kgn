<section id="slider">
    <div class="starter_container bg">
        <div class="container">
            <div class="slider_container">
                <div class="row">
                    <div class="col-md-8">
                        <h2>Новости Красноярска</h2>
                        <?php if(isset($slider)) { echo $slider; } ?>
                    </div>
                    <div class="col-md-4" id="popular">
                        <?php if(isset($popular)) { ?>
                            <h3>Популярное</h3><?php
                            foreach($popular as $p => $l) { ?>
                                <p>
                                    <a title="<?= $l->name; ?>" href="<?= URL::base(); ?>news/item/<?= $l->url; ?>.html"><?= $l->name; ?></a>
                                </p><?php
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>