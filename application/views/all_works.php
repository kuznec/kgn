<section id="last_works">
    <div class="container">
        <div class="row">
            <div class="row-md-12">
                <h1>Портфолио</h1>
            </div>
        </div>
        <div class="row"><?php
            foreach ($works as $k => $v) { ?>
                <div class="col-md-4 item">
                    <a href="<?= URL::base(); ?>panorama/<?= $v->translit; ?>/index.html">
                    <div class="img_conteiner">
                        <img src="<?= URL::base() ?>public/works/<?= $v->id; ?>/<?= $v->photo; ?>">
                        <div class="img_hover3">
                            <div class="img_hover"></div>
                            <div class="img_hover2">
                                
                                <div class="container_table">
                                    <div class="container_ceil">
                                        <img class="whorig" src="<?= URL::base(); ?>public/images/lg.png">Посмотреть виртуальный тур
                                    </div>
                                </div>
                                
                            </div>
                        </div>    
                    </div>
                    </a>
                    <h3><?= $v->slide_h; ?></h3>
                    <p><?= $v->slide_descr; ?></p>
                </div><?php
            } ?>
        </div>
    </div>
</section>