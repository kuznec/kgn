<script src="<?= URL::base(); ?>public/js/bootstrap.min.js" type="text/javascript"></script>
<article class="content item-editor-page">
    <div class="tab_container">
        <form name="item" method="POST">
            <div class="card card-block"><?php
                foreach($slide as $k => $v)
                { ?>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h1:</label>
                                <input type="text" name="h1" class="form-control boxed" value="<?= $v->h1; ?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text1:</label>
                                <textarea class="form-control boxed" id="text" name="text1"><?= $v->text1; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h2:</label>
                                <input type="text" name="h2" class="form-control boxed" value="<?= $v->h2; ?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text2:</label>
                                <textarea class="form-control boxed" name="text2"><?= $v->text2; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h3:</label>
                                <input type="text" name="h3" class="form-control boxed" value="<?= $v->h3; ?>">
                            </div>
                            
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text3:</label>
                                <textarea class="form-control boxed" name="text3"><?= $v->text3; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" value="<?= $v->id; ?>">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </div><?php
                } ?>
            </div>
        </form>
    </div>
</article>
<script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('text');
});
</script>