<section class="post">
    <div class="container">
        <div class="breadcrumb"><a href="<?= URL::base(); ?>">Главная</a> » <a href="<?= URL::base()."news"; ?>">Новости</a></div>
        <div class="row">
            <article class="article">
                <div class="col-sm-12 col-md-6 col-lg-3 img-holder"><?php
                    if(!empty($img_name))
                    { ?>
                        <img src="<?= URL::base(); ?>public/images/news/<?= $year; ?>/<?= $month; ?>/<?= $img_name; ?>" alt="<?= $name; ?>"><?php
                    } ?>
                    <p class="date"><?= date('d-m-Y',strtotime($date)); ?></p>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-9 post-cnt">
                    <h1><?= $name; ?></h1>
                    <?= $text; ?>
                    <?php
                    if($tag) { ?>
                    <p>Рубрика: <a href="<?= URL::base('http'); ?>news/rubric/<?= $tag; ?>"><?= $rubs_name; ?></a></p><?php
                    }

                    if($link) { ?>
                    <noindex><p>Источник: <?= $link; ?></p></noindex><?php
                    } ?>
                </div>
            </article>
        </div>
    </div>
</section>