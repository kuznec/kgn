<section class="post all_post">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= $name; ?></h1>
            </div>
        </div><?php
        foreach($news as $k => $v)
        { ?>
        <article class="article">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-3 img-holder"><?php
                    if(!empty($v->img_name))
                    { ?>
                        <a href="<?= URL::base(); ?>news/item/<?= $v->url; ?>.html">
                            <img src="<?= URL::base(); ?>public/images/news/<?= $v->year; ?>/<?= $v->month; ?>/<?= $v->img_name; ?>" alt="<?= $v->name; ?>">
                        </a><?php
                    } ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-9 post-cnt">
                    <a class="article-hitem" href="<?= URL::base(); ?>news/item/<?= $v->url; ?>.html"><?= $v->name; ?></a><?php if($v->tag) { ?>
                    <div class="rubrica">Рубрика: <a href="<?= URL::base(); ?>news/rubric/<?= $v->tag; ?>"><?= $v->rubs_name; ?></a></div><?php } ?>
                    <?= substr($v->text, 0, stripos($v->text, "</p>")+4); ?>
                    <p class="text-right"><a href="<?= URL::base(); ?>news/item/<?= $v->url; ?>.html">Подробнее..</a></p>
                </div>
            </div>
        </article><?php
        } ?>
    </div>
</section>