<?php if(!empty($pages)) 
{
    foreach($pages as $k => $v)
    {
        $active = "";
        if($v->translit == '') {
            if(Request::$current->controller() == 'Static' && !Request::$current->param('page'))
                $active = "navactive";
        }
        else {
            if(strtolower(Request::$current->controller()) == $v->translit)
                $active = "navactive";
            elseif(Request::$current->param('page') == $v->translit)
                $active = "navactive";
        } ?>
        <li><a class="<?= $active; ?> color_animation" href="<?= URL::base().$v->translit; ?>"><?= $v->name_menu; ?></a></li><?php
    }
} ?>